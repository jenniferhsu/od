od~
desc: pd analysis external that detects transients by finding the spectral energy of each STFT frame (512-point FFT, 50% overlap, Hamming window) and analyzing the high frequency content (HFC) 

for ucsd music 267 course

author: jennifer hsu (jsh008@ucsd.edu)
date: fall 2013
