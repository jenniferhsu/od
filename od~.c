// ================================================================================
// name: od~.c
// desc: extern for pd
//          onset detection with stft
//          stft operates on windows with size 512 and 50% overlap with a Hamming window
//          onsets are found by:
//              * taking the STFT
//              * calculating the energy of the STFT frame
//              * calculating the energy of the STFT frame weighted by the frequency bin
//              * calculating a measure of transientness (MOTR)
//              * comparing the MOTR with some user supplied threshold
//              * checking that onsets do not occur to close to one another
//
//
// fft code at bottom from musicdsp.org by Toth Laszlo
// see: http://musicdsp.org/files/rvfft.cpp and http://musicdsp.org/files/rvfft.ps
// adjusted for floats and for pi
//
//
// author: jennifer hsu
// date: fall 2013
// =================================================================================

#include "m_pd.h"
#include <stdlib.h>
#include <math.h>
#include <string.h>

// derived from some tests, used to normalize measure of transientness (MOTR) to 1
#define MOTR_MAX 3.5f


// struct type to hold all variables that have to do with the od~ object
typedef struct _od
{
    t_object x_obj;
    
    t_float lastHFC;                    // keep track of last high frequency content value
    t_float thresh;                     // threshold (user parameter)
    t_int numFrames;                    // number of consecutive frames to disregard transients (user parameter)
    t_outlet * bangOutlet;              // bangs when an onset is detected
    t_int framesSinceLastTransient;     // counts the number of frames since the last detected onset
    
    // stft variables
    t_float sr;                         // sample rate
    t_float * delayedIn;                // hold delayed input
    t_int delayedInSize;
    t_int dWrite;
    t_float * fftBuffer;                // hold input to pass to fft function
    t_int fftSize;
    t_int fftRead;
    t_int fftWrite;
    t_float * win;                      // window for STFT
    t_int winSize;
    t_int winCntr;
    
} t_od;

// od~ class that will be created in 'setup'
// and used in 'new' to create new instances
t_class *od_class;

/* function prototypes */
static t_int *od_perform(t_int *w);
static void od_dsp(t_od *x_obj, t_signal **sp);
void od_setNumFrames(t_od *x, t_floatarg f);
static void *od_new(void);
void od_tilde_setup(void);
// FFT functions
void realfft_split(float *data,long n);
void realfft_split_unshuffled(float *data,long n);
void irealfft_split(float *data,long n);


// perform function (audio callback)
static t_int *od_perform(t_int *w)
{
    
    // extract data
    t_od *x = (t_od *)(w[1]);
    t_float *in = (t_float *)(w[2]);
    t_float *out = (t_float *)(w[3]);
    int bufferSize = (int)(w[4]);
    
    t_int fftSizeOver2 = x->fftSize/2;
    
    int samp;
    // copy into delay buffer at correct point
    for(samp = 0; samp < bufferSize; samp++)
    {
        // fill in FFT buffer with input samples
        // *(x->fftBuffer+samp) = *(in+samp);
        
        // copy delayed input
        *(x->delayedIn+x->dWrite) = *(in+samp);
        
        // increment readers/writers and set to correct positions
        x->dWrite++;
        // it seems unsafe to increment without checking the value/wrapping
        // but I want this to be at an exact value for the STFT
        
    }
    
    // ============== STFT =====================================
    
    if(x->dWrite == fftSizeOver2)
    {
        
        // copy delayed signal into fft/ifft buffer
        memcpy(x->fftBuffer, x->delayedIn+(fftSizeOver2), (fftSizeOver2)*sizeof(t_float));
        memcpy(x->fftBuffer+(fftSizeOver2), x->delayedIn, (fftSizeOver2)*sizeof(t_float));
        
        
        // window the signal
        int i;
        for(i=0; i < x->fftSize; i++)
        {
            *(x->fftBuffer+i) *= *(x->win+i);
        }
        
        // take the fft
        realfft_split_unshuffled(x->fftBuffer, x->fftSize);
    }
    
    
    if(x->dWrite == x->fftSize)
    {
        
        // copy the stuff into the ifft buffer
        memcpy(x->fftBuffer, x->delayedIn, x->fftSize*sizeof(t_float));
        
        // window the signal
        int i;
        for(i=0; i < x->fftSize; i++)
        {
            *(x->fftBuffer+i) *= *(x->win+i);
        }
        
        // take the fft
        realfft_split_unshuffled(x->fftBuffer, x->fftSize);
    }
    
    // ============================================================
    

    // === onset detection calculations ===
    t_float motr = 0.0f;
    
    if(x->dWrite == fftSizeOver2 || x->dWrite == x->fftSize)
    {
        // calculate energy and high frequency content
        // discard 2 lowest bins to avoid unwanted DC component
        // energy = sum_k(|X(k)|^2) for fft bin k
        // HFC = sum_k(k*|X(k)|^2) for fft bin k
        t_float energy = 0.0f;
        t_float hfc = 0.0f;
        t_float mag;
        for(samp = 2; samp < bufferSize; samp+=2)
        {
        
            // (mag(re + j*im))^2 = re^2 + im^2
            // signal reduction
            mag = (*(x->fftBuffer+samp) * *(x->fftBuffer+samp)) + (*(x->fftBuffer+(samp+1)) * *(x->fftBuffer+(samp+1)));
        
            energy += mag;
            hfc += samp*mag;
            
        }
        
        // constrict energy and hfc to have minimum value of 1 so we don't divide by 0
        // my solution is to add 1 to their values
        energy += 1.0f;
        hfc += 1.0f;
        
        // onset detection
        motr = (hfc / x->lastHFC) * (hfc / energy);
        
        x->lastHFC = hfc;

        // check against threshold
        if( motr/MOTR_MAX > x->thresh )
        {
            // check number of frames
            if( x->framesSinceLastTransient >= x->numFrames )
            {
                // we got a transient so let out a bang
                outlet_bang(x->bangOutlet);
                x->framesSinceLastTransient = 0;
            } else
            {
                x->framesSinceLastTransient++;
            }
        } else
        {
            x->framesSinceLastTransient++;
        }
    }
    // =======================================
    
    // increment pointers for stft
    while(x->dWrite >= x->delayedInSize)
        x->dWrite -= x->delayedInSize;
    while(x->dWrite < 0)
        x->dWrite += x->delayedInSize;
    
    for(samp = 0; samp < bufferSize; samp++)
    {
        *(out+samp) = motr/MOTR_MAX;
    }
    
    return (w+5);
}

// called to start dsp, register perform function
static void od_dsp(t_od *x, t_signal **sp)
{
    x->sr = sp[0]->s_sr;
    // add the perform function
    dsp_add(od_perform, 4, x, sp[0]->s_vec, sp[1]->s_vec, sp[0]->s_n);
}


// function to set the number of consecutive frames to disregard a transient
void od_setNumFrames(t_od *x, t_floatarg f)
{
    x->numFrames = ( int )f;
}


// clean up memory
static void od_free(t_od *x)
{
	free(x->fftBuffer);
    free(x->delayedIn);
    free(x->fftBuffer);
    free(x->win);
}


// new function that is called everytime we create a new od object in pd
static void *od_new(void)
{
    // create object from class
    t_od *x = (t_od *)pd_new(od_class);
    
    // create an inlet to set the number of frames to disregard transients
    inlet_new(&x->x_obj, &x->x_obj.ob_pd, gensym("float"), gensym("numFrames"));
    
    // create a signal outlet for the measure of transientness signal
    outlet_new(&x->x_obj, gensym("signal"));
    
    // create a bang outlet for when we see a transient
    x->bangOutlet = outlet_new(&x->x_obj, gensym("bang"));
    
    // onset detection variables
    x->lastHFC = 1.0f;
    x->thresh = 0.5f;
    x->numFrames = 2;
    x->framesSinceLastTransient = x->numFrames + 1; // so we catch the first transient
    
    // initialize stft variables
    x->sr = 44100.0f;   // this will get set by Pd
    
    x->fftSize = 512;
    x->delayedInSize = 512;
    
    // buffer indices
    x->dWrite = 0;
    x->fftWrite = 0;
    x->fftRead = 0;
    
    x->delayedIn = (t_float *)malloc(sizeof(t_float) * x->delayedInSize);
    x->fftBuffer = (t_float *)malloc(sizeof(t_float) * x->delayedInSize);
    
    // zero out buffers
    int i;
    for(i = 0; i < x->delayedInSize; i++)
    {
        *(x->delayedIn+i) = 0.0f;
        *(x->fftBuffer+i) = 0.0f;
    }
    
    // window
    x->winSize = x->fftSize;
    x->winCntr = 0;
    x->win = (t_float *)malloc(x->winSize*sizeof(t_float));
    t_float alpha = 0.54;
    t_float beta = 0.46;
    for(i = 0; i < x->winSize; i++)
    {
        *(x->win+i) = alpha - beta*cos(2.0f*M_PI*(( float )i/( float )x->winSize));
    }
    
    return (x);
}

// called when Pd is first loaded and tells Pd how to load the class
void od_tilde_setup(void)
{
    od_class = class_new(gensym("od~"), (t_newmethod)od_new, (t_method)od_free, sizeof(t_od), 0, 0);
    
    // magic to declare the leftmost inlet the main inlet that will take a signal
    // installs delayTime as the leftmost inlet float
    CLASS_MAINSIGNALIN(od_class, t_od, thresh);
    
    // register method that will be called when dsp is turned on
    class_addmethod(od_class, (t_method)od_dsp, gensym("dsp"), (t_atomtype)0);
    
    // register callback method for right inlet for number of frames to disregard (float)
    class_addmethod(od_class, (t_method)od_setNumFrames, gensym("numFrames"), A_FLOAT, 0);
    
}


/* ============================ */
// FFT METHODS FROM MUSICDSP.ORG
/* ============================ */

/////////////////////////////////////////////////////////
// Sorensen in-place split-radix FFT for real values
// data: array of doubles:
// re(0),re(1),re(2),...,re(size-1)
//
// output:
// re(0),re(1),re(2),...,re(size/2),im(size/2-1),...,im(1)
// normalized by array length
//
// Source:
// Sorensen et al: Real-Valued Fast Fourier Transform Algorithms,
// IEEE Trans. ASSP, ASSP-35, No. 6, June 1987

void realfft_split(float *data,long n){
    
    long i,j,k,i5,i6,i7,i8,i0,id,i1,i2,i3,i4,n2,n4,n8;
    double t1,t2,t3,t4,t5,t6,a3,ss1,ss3,cc1,cc3,a,e,sqrt2;
    
    sqrt2=sqrt(2.0);
    n4=n-1;
    
    //data shuffling
    for (i=0,j=0,n2=n/2; i<n4 ; i++){
        if (i<j){
            t1=data[j];
            data[j]=data[i];
            data[i]=t1;
        }
        k=n2;
        while (k<=j){
            j-=k;
            k>>=1;
        }
        j+=k;
    }
	
    /*----------------------*/
	
	//length two butterflies
	i0=0;
	id=4;
    do{
        for (; i0<n4; i0+=id){
			i1=i0+1;
			t1=data[i0];
			data[i0]=t1+data[i1];
			data[i1]=t1-data[i1];
		}
        id<<=1;
        i0=id-2;
        id<<=1;
    } while ( i0<n4 );
    
    /*----------------------*/
    //L shaped butterflies
    n2=2;
    for(k=n;k>2;k>>=1){
        n2<<=1;
        n4=n2>>2;
        n8=n2>>3;
        e = 2.0f*M_PI/(n2);
        i1=0;
        id=n2<<1;
        do{
            for (; i1<n; i1+=id){
                i2=i1+n4;
                i3=i2+n4;
                i4=i3+n4;
                t1=data[i4]+data[i3];
                data[i4]-=data[i3];
                data[i3]=data[i1]-t1;
                data[i1]+=t1;
                if (n4!=1){
                    i0=i1+n8;
                    i2+=n8;
                    i3+=n8;
                    i4+=n8;
                    t1=(data[i3]+data[i4])/sqrt2;
                    t2=(data[i3]-data[i4])/sqrt2;
                    data[i4]=data[i2]-t1;
                    data[i3]=-data[i2]-t1;
                    data[i2]=data[i0]-t2;
                    data[i0]+=t2;
                }
            }
            id<<=1;
            i1=id-n2;
            id<<=1;
        } while ( i1<n );
        a=e;
        for (j=2; j<=n8; j++){
            a3=3*a;
            cc1=cos(a);
            ss1=sin(a);
            cc3=cos(a3);
            ss3=sin(a3);
            a=j*e;
            i=0;
            id=n2<<1;
            do{
                for (; i<n; i+=id){
                    i1=i+j-1;
                    i2=i1+n4;
                    i3=i2+n4;
                    i4=i3+n4;
                    i5=i+n4-j+1;
                    i6=i5+n4;
                    i7=i6+n4;
                    i8=i7+n4;
                    t1=data[i3]*cc1+data[i7]*ss1;
                    t2=data[i7]*cc1-data[i3]*ss1;
                    t3=data[i4]*cc3+data[i8]*ss3;
                    t4=data[i8]*cc3-data[i4]*ss3;
                    t5=t1+t3;
                    t6=t2+t4;
                    t3=t1-t3;
                    t4=t2-t4;
                    t2=data[i6]+t6;
                    data[i3]=t6-data[i6];
                    data[i8]=t2;
                    t2=data[i2]-t3;
                    data[i7]=-data[i2]-t3;
                    data[i4]=t2;
                    t1=data[i1]+t5;
                    data[i6]=data[i1]-t5;
                    data[i1]=t1;
                    t1=data[i5]+t4;
                    data[i5]-=t4;
                    data[i2]=t1;
                }
                id<<=1;
                i=id-n2;
                id<<=1;
            } while(i<n);
        }
    }
    
	//division with array length
    for(i=0;i<n;i++) data[i]/=n;
}


/////////////////////////////////////////////////////////
// Sorensen in-place inverse split-radix FFT for real values
// data: array of doubles:
// re(0),re(1),re(2),...,re(size/2),im(size/2-1),...,im(1)
//
// output:
// re(0),re(1),re(2),...,re(size-1)
// NOT normalized by array length
//
// Source:
// Sorensen et al: Real-Valued Fast Fourier Transform Algorithms,
// IEEE Trans. ASSP, ASSP-35, No. 6, June 1987

void irealfft_split(float *data,long n)
{
    
    long i,j,k,i5,i6,i7,i8,i0,id,i1,i2,i3,i4,n2,n4,n8,n1;
    double t1,t2,t3,t4,t5,a3,ss1,ss3,cc1,cc3,a,e,sqrt2;
    
    sqrt2=sqrt(2.0);
    
    n1=n-1;
    n2=n<<1;
    for(k=n;k>2;k>>=1){
        id=n2;
        n2>>=1;
        n4=n2>>2;
        n8=n2>>3;
        e = 2.0f*M_PI/(n2);
        i1=0;
        do{
            for (; i1<n; i1+=id){
                i2=i1+n4;
                i3=i2+n4;
                i4=i3+n4;
                t1=data[i1]-data[i3];
                data[i1]+=data[i3];
                data[i2]*=2;
                data[i3]=t1-2*data[i4];
                data[i4]=t1+2*data[i4];
                if (n4!=1){
                    i0=i1+n8;
                    i2+=n8;
                    i3+=n8;
                    i4+=n8;
                    t1=(data[i2]-data[i0])/sqrt2;
                    t2=(data[i4]+data[i3])/sqrt2;
                    data[i0]+=data[i2];
                    data[i2]=data[i4]-data[i3];
                    data[i3]=2*(-t2-t1);
                    data[i4]=2*(-t2+t1);
                }
            }
            id<<=1;
            i1=id-n2;
            id<<=1;
        } while ( i1<n1 );
        a=e;
        for (j=2; j<=n8; j++){
            a3=3*a;
            cc1=cos(a);
            ss1=sin(a);
            cc3=cos(a3);
            ss3=sin(a3);
            a=j*e;
            i=0;
            id=n2<<1;
            do{
                for (; i<n; i+=id){
                    i1=i+j-1;
                    i2=i1+n4;
                    i3=i2+n4;
                    i4=i3+n4;
                    i5=i+n4-j+1;
                    i6=i5+n4;
                    i7=i6+n4;
                    i8=i7+n4;
                    t1=data[i1]-data[i6];
                    data[i1]+=data[i6];
                    t2=data[i5]-data[i2];
                    data[i5]+=data[i2];
                    t3=data[i8]+data[i3];
                    data[i6]=data[i8]-data[i3];
                    t4=data[i4]+data[i7];
                    data[i2]=data[i4]-data[i7];
                    t5=t1-t4;
                    t1+=t4;
                    t4=t2-t3;
                    t2+=t3;
                    data[i3]=t5*cc1+t4*ss1;
                    data[i7]=-t4*cc1+t5*ss1;
                    data[i4]=t1*cc3-t2*ss3;
                    data[i8]=t2*cc3+t1*ss3;
                }
                id<<=1;
                i=id-n2;
                id<<=1;
            } while(i<n1);
        }
	}	
    
    /*----------------------*/
	i0=0;
	id=4;
    do{
        for (; i0<n1; i0+=id){ 
			i1=i0+1;
			t1=data[i0];
			data[i0]=t1+data[i1];
			data[i1]=t1-data[i1];
		}
        id<<=1;
        i0=id-2;
        id<<=1;
    } while ( i0<n1 );
    
    /*----------------------*/
    
    //data shuffling
    for (i=0,j=0,n2=n/2; i<n1 ; i++){
        if (i<j){
            t1=data[j];
            data[j]=data[i];
            data[i]=t1;
        }
        k=n2;
        while (k<=j){
            j-=k;
            k>>=1;	
        }
        j+=k;
    }	
}


/////////////////////////////////////////////////////////
// Sorensen in-place split-radix FFT for real values
// data: array of doubles:
// re(0),re(1),re(2),...,re(size-1)
//
// output:
// re(0),re(size/2),re(1),im(1),re(2),im(2),...,re(size/2-1),im(size/2-1)
// normalized by array length
//
// Source:
// Source: see the routines it calls ...

void realfft_split_unshuffled(float *data,long n)
{
    
	float *data2;
	long i,j;
    
	realfft_split(data,n);
	//unshuffling - not in-place
	data2=(float *)malloc(n*sizeof(float));
	j=n/2;
	data2[0]=data[0];
	data2[1]=data[j];
	for(i=1;i<j;i++) {data2[i+i]=data[i];data2[i+i+1]=data[n-i];}
	for(i=0;i<n;i++) data[i]=data2[i];
	free(data2);
}



